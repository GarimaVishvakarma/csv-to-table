from django.conf.urls import url
from django.contrib import admin
from .views import (index, details, view_files)


urlpatterns = [
    url(r'^$', index, name="index"),
    url(r'^details/$', details, name="details"),
    url(r'^list/(?P<slug>[-\w]+)', view_files, name="list-details"),
]

