from django.db import models

# Create your models here.
class StudentDetails(models.Model):
	student_id = models.CharField(max_length=10)
	student_name = models.CharField(max_length=255)
	date_of_birth = models.CharField(max_length=255)
	department = models.CharField(max_length=255)
	student_email = models.EmailField(max_length=255)
	student_contact = models.CharField(max_length=10)

	def __str__(self):
		return self.student_id + " - "+ self.student_name