from __future__ import absolute_import, unicode_literals
from django.utils.translation import ugettext_lazy as _

from celery import shared_task

from django.db import connection


@shared_task
def insert_csv_data(filename, data):
	"""
	Insert Data In Table
	"""
	try:
		cursor = connection.cursor()
		query = "Insert Into "+str(filename) + " values "
		for row in data:
			if row != "":
				cols = row.split(",")
				query += " ( "
				for col in cols:
					query += "'"+col + "',"
				query = query[:-1] +" ),"
		query = query[:-1]
		cursor.execute(query)
	except Exception as e:
		return False
	return True