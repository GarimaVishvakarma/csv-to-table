from django.db import connection

def create_csv_table(filename, data, has_header):
	"""
	Create Table Of CSV Files
	"""
	try:
		cursor = connection.cursor()
		if has_header == True:
			query = "Create table "+ str(filename) + " ( "
			for header in data.split(","):
				query += str(header.lower().replace(" ","_")) + " varchar(255) ,"
			query = query[:-1] + ")"
			cursor.execute(query)
		else:
			length = len(data.split(","))
			query = "Create table "+ str(filename) + " ( "
			for header in range(1, length+1):
				query += "field"+ str(header) + " varchar(255) ,"
			query = query[:-1] + ")"
			cursor.execute(query)
	except :
		return False
	return True

def fetch_data_from_table(name):
	"""
	Fetch Data to Show in Table
	"""
	result = {}
	try :
		cursor = connection.cursor()
		query = "select column_name from information_schema.columns where table_name = '"+str(name)+"'"
		cursor.execute(query)
		data = cursor.fetchall()
		rows = cursor.rowcount
		query2 = "select * from "+str(name);
		cursor.execute(query2)
		data2 = cursor.fetchall()
		result["columns"] = data
		result["data"] = data2
		result["rows"] = range(rows)
		return result
	except Exception as e:
		return e
	return True
