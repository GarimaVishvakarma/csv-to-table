from django.shortcuts import render
import csv
from .utils import create_csv_table,  fetch_data_from_table
from .tasks import insert_csv_data
from django.core.files.storage import FileSystemStorage
import os
from django.conf import settings
from django.contrib import messages


def index(request):
    """
    Page To Upload a file.
    """
    if request.method == "POST":
        data = request.FILES["csvfile"].read().decode("utf-8")
        
        sniffer = csv.Sniffer()
        has_header = sniffer.has_header(data)

        data = data.split("\n")
        if has_header:
            """ Create Table if header name is present in csv file"""
            create_table = create_csv_table(str(request.FILES["csvfile"]).split(".")[0], data[0], True)
        else:
            """Create Table if header name not Present in csv file"""
            len_data = len(data[0])
            create_table = create_csv_table(str(request.FILES["csvfile"]).split(".")[0], data[0], False)
        if create_table:
            """Insert Data In Table"""
            insert_csv_data.delay(str(request.FILES["csvfile"]).split(".")[0], data[1:])
            """Upload File"""
            fs = FileSystemStorage()
            filename = fs.save(request.FILES["csvfile"].name, request.FILES["csvfile"])
            fs.url(filename)
            message = 'Your File has been Uploaded Successfully !'
            messages.add_message(request, messages.INFO, message)
        else:
            message = "Unable to Upload File !"
            messages.add_message(request, messages.WARNING, message)
    return render(request, "index.html")

def details(request):
    """List Of All the files uploaded"""
    files = os.listdir(settings.BASE_DIR+"/media/")
    context = {"files" : files}
    return render(request, "details.html", context)



def view_files(request, slug):
    """View Data of File along with searching functionality"""
    name = slug.split(".")[0]
    data = fetch_data_from_table(name)
    return render(request, "table.html",{"data": data})
