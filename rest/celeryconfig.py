# CELERY CONFIGURATION
BROKER_TRANSPORT = 'redis'
BROKER_URL = 'amqp://localhost:5672/0'
CELERY_RESULT_BACKEND = 'amqp://localhost:5672/0'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
